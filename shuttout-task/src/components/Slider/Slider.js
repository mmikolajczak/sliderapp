import React, { useContext } from 'react';
import './Slider.scss';
import { MyContext } from '../../context/MyContext';

function Slider() {
  const context = useContext(MyContext);
  const slides = context.slides;
  const curr = context.curr;

  return (
    <div className="slider__container">
      {slides.map((s, i) => (
        <div
          className={i === curr ? "active slider__container__wrap" : "slider__container__wrap"}
          key={s.title}
          aria-hidden={i !== curr}>
          {i === curr && (
            <img className="slider__container__wrap__image" src={s.image} alt={s.title} />
          )}
          <div className="slider__container__wrap__content">
            <h1>{s.title}</h1>
            <h2>{s.category}</h2>
            <p>{s.description}</p>
          </div>
          <ul>
            {slides.map((s, i) => (
              <li key={i}>
                <span
                  className={i === curr ? "active" : ""}
                  onClick={() => {
                    context.setCurr(i);
                  }}>
                  {i}
                </span>
              </li>
            ))}
          </ul>
        </div>
      ))}
    </div>
  );
}

export default Slider;
