import React from 'react';
import './App.scss';
import Slider from '../Slider/Slider';
import SliderTool from '../SliderTool/SliderTool';
import MyContextProvider from '../../context/MyContext'

function App() {
  return (
    <MyContextProvider>
      <SliderTool />
      <Slider />
    </MyContextProvider>
  );
}

export default App;
