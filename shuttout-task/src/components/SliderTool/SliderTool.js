import React, { useState, useRef, useContext } from 'react';
import './SliderTool.scss';
import { MyContext } from '../../context/MyContext';

function SliderTool() {
  const context = useContext(MyContext);
  const setSlides = context.setSlides;
  const slidesTemp = context.slidesTemp;
  const setEditSlide = context.setEditSlide;
  const editedSlide = context.editedSlide;
  const filters = context.filters;
  const setSlidesTemp = context.setSlidesTemp;
  const setCurr = context.setCurr;
  const setActiveFilter = context.setActiveFilter;
  const activeFilter = context.activeFilter;

  const [isOpenModal, openModal] = useState(false);
  const [isShow, setShow] = useState(false);
  const [option, setOption] = useState('');
  const fileInput = useRef(null);

  const handleOperation = (s, option) => {
    const findSlide = (slide) => {
      return slide.id === s.id;
    };
    switch (option) {
      case 'delete':
        setSlidesTemp(slidesTemp.filter(el => el.id !== s.id));
        handleFilter(null);
        clear();
        break;
      case 'edit':
        let findObject = slidesTemp.find(findSlide);
        setEditSlide(findObject);
        setShow(true);
        setOption(option);
        break;
      default:
        break;
    };
  };

  const handleChange = (e, name) => {
    if (name === 'image') {
      new Promise(resolve => {
        let fr = new FileReader();
        fr.onload = () => resolve(fr.result);
        fr.readAsDataURL(fileInput.current.files[0])
      }).then(response => {
        editedSlide[name] = response
        setEditSlide({ ...editedSlide });
      })
    } else {
      editedSlide[name] = e.target.value;
      setEditSlide({ ...editedSlide });
    }
  }

  const updateSlide = () => {
    const findSlide = (slide) => {
      return slide.id === editedSlide.id;
    };
    let findIndex = slidesTemp.findIndex(findSlide);
    slidesTemp[findIndex] = editedSlide;
    setSlidesTemp([...slidesTemp]);
    clear();
  }

  const addSlider = () => {
    let ids = [];
    for (let i = 0; i < slidesTemp.length; i++) {
      ids.push(slidesTemp[i].id);
    }
    let slide = { ...editedSlide };
    slide.id = Math.max(...ids) + 1;
    slidesTemp.push(slide);
    setSlidesTemp([...slidesTemp]);
    clear();
  }

  const add = () => {
    setOption('add');
    setShow(true);
  }

  const close = () => {
    openModal(false);
    clear();
  }

  const clear = () => {
    setShow(false);
    setOption('');
    setEditSlide({
      title: '',
      category: '',
      description: '',
      image: null
    });
    setCurr(0);
  }

  const handleFilter = (e) => {
    let newSlides = [];
    if (e == null || e.target.value === 'All') {
      newSlides = [...slidesTemp];
      setActiveFilter('All');
    } else {
      for (let i in slidesTemp) {
        if (slidesTemp[i].category === e.target.value) {
          newSlides.push(slidesTemp[i]);
        }
      }
      setActiveFilter(e.target.value);
    }
    setCurr(0);
    setSlides([...newSlides]);
  }

  return (
    <div className="slidertool__container">
      <select onClick={(e) => handleFilter(e)}>
        {filters.map((filter, i) => (
          <option key={i} value={filter} selected={filter === activeFilter}>{filter}</option>
        ))}
      </select>
      <button
        id="tool_button"
        className="slidertool__container__button slidertool__container__button--tool"
        onClick={() => openModal(true)}>
        <i className="fas fa-tools"></i>
      </button>
      {isOpenModal && <div className="slidertool__container__modal">
        <div className="slidertool__container__modal__content">
          <button
            className="slidertool__container__modal__content--close"
            onClick={() => close()}>
            &times;
          </button>
          {isShow && <>
            <fieldset>
              <legend>Form</legend>
              <form id="SliderForm" className="slidertool__container__modal__content__form">
                <label htmlFor="title">Title</label>
                <input
                  id="title"
                  className="slidertool__container__modal__content__form--input"
                  type="text" value={editedSlide.title}
                  onChange={(e) => handleChange(e, 'title')} />
                <label htmlFor="category">Category</label>
                <input
                  id="category"
                  className="slidertool__container__modal__content__form--input"
                  type="text"
                  value={editedSlide.category}
                  onChange={(e) => handleChange(e, 'category')} />
                <label htmlFor="description">Description</label>
                <textarea
                  rows="4"
                  cols="50"
                  id="description"
                  className="slidertool__container__modal__content__form--input"
                  type="text"
                  value={editedSlide.description}
                  onChange={(e) => handleChange(e, 'description')} />
                <label htmlFor="uploadImage">Upload Image</label>
                <input
                  id="uploadImage"
                  ref={fileInput}
                  className="slidertool__container__modal__content__form--input"
                  type="file"
                  accept="image/x-png,image/gif,image/jpeg"
                  onChange={(e) => handleChange(e, 'image')} />
                {editedSlide.image &&
                  <img className="slidertool__container__modal__content__form__image" src={editedSlide.image} alt={editedSlide.title + editedSlide.id} />}
              </form>
              {option === 'edit' ?
                <button
                  id="update__button"
                  className="slidertool__container__modal__content__button"
                  onClick={() => updateSlide()}>
                  Update
              </button>
                :
                <button
                  id="add__button"
                  className="slidertool__container__modal__content__button"
                  onClick={() => addSlider()}>
                  Save
              </button>
              }
              <button
                id="cancel__button"
                className="slidertool__container__modal__content__button"
                onClick={() => clear()}>
                Cancel
              </button>
            </fieldset>
          </>}
          {option === '' &&
            <button
              id="add__button"
              className="slidertool__container__modal__content__button"
              onClick={() => add()}>
              Add
            </button>}
          <table>
            <thead>
              <tr>
                <th>Title</th>
                <th>Category</th>
                <th>Description</th>
                <th>Image</th>
                <th>Options</th>
              </tr>
            </thead>
            <tbody>
              {slidesTemp.map((s, i) => (
                <tr key={i}>
                  <td>{s.title}</td>
                  <td>{s.category}</td>
                  <td>{s.description}</td>
                  <td><img src={s.image} alt={s.title} /></td>
                  <td>
                    <button
                      id="delete__button"
                      className="delete"
                      onClick={() => handleOperation(s, 'delete')}>
                      Delete
                    </button>
                    <button
                      id="edit__button"
                      className="edit"
                      onClick={() => handleOperation(s, 'edit')}>
                      Edit
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>}
    </div>
  );
}

export default SliderTool;
