export default [
    {
        id: 0,
        title: 'First slide!',
        category: 'is it working?',
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Pellentesque gravida tincidunt lorem, ut volutpat nibh finibus ac.
        Proin est elit, tempus a viverra at, faucibus eu ipsum.`,
        image: 'https://picsum.photos/300',
    },
    {
        id: 1,
        title: "Second Slide",
        category: "ohhh yeah",
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Pellentesque gravida tincidunt lorem, ut volutpat nibh finibus ac.
        Proin est elit, tempus a viverra at, faucibus eu ipsum.`,
        image: "https://picsum.photos/400"
    },
    {
        id: 2,
        title: "Third Slide",
        category: "last one! ✨",
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Pellentesque gravida tincidunt lorem, ut volutpat nibh finibus ac.
        Proin est elit, tempus a viverra at, faucibus eu ipsum.`,
        image: "https://picsum.photos/350"
    },
]