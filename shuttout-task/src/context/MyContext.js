import React, { useState, useEffect, createContext } from 'react';
import defaultSlides from './defaultSlides';

export const MyContext = createContext();

const MyContextProvider = (props) => {
    const [slides, setSlides] = useState(defaultSlides);
    const [slidesTemp, setSlidesTemp] = useState([]);
    const [filters, setFilters] = useState([]);
    const [activeFilter, setActiveFilter] = useState('');
    const [curr, setCurr] = useState(0);
    const [editedSlide, setEditSlide] = useState({
        title: '',
        category: '',
        description: '',
        image: null
    });
    const { length } = slides;

    useEffect(() => {
        if (slidesTemp.length === 0) {
            setSlidesTemp([...slides]);
        } else if (slidesTemp.length !== 0) {
            let filters = [];
            let result = [];
            filters.push('All');
            for (let i in slidesTemp) {
                filters.push(slidesTemp[i].category);
            }
            filters.forEach(function (a) {
                if (!this[a]) {
                    this[a] = a;
                    result.push(this[a]);
                }
            }, Object.create(null));
            setFilters(result);
        }
    }, [slides, slidesTemp])

    useEffect(() => {
        if (localStorage.getItem('shuttoutdata')) {
            let data = JSON.parse(localStorage.getItem('shuttoutdata'));
            setCurr(data.curr);
            setActiveFilter(data.activeFilter);
            setSlides(data.slides);
            setSlidesTemp(data.slidesTemp);
        }
    }, [])

    useEffect(() => {
        saveDataToStorage();
        if (slides.length !== 0 && slides.length > 1) {
            let myInterval = setInterval(
                goToNext
                , 2000);
            return function () {
                clearInterval(myInterval);
            }
        }
    })

    const saveDataToStorage = () => {
        localStorage.removeItem('shuttoutdata');
        let data = {
            curr,
            activeFilter,
            slides,
            slidesTemp
        };
        localStorage.setItem('shuttoutdata', JSON.stringify(data));
    }

    const goToNext = () => {
        let setNumber = 0;
        if (curr !== length - 1) {
            setNumber = curr + 1;
        }
        setCurr(setNumber);
    }

    if (!Array.isArray(slides) || length <= 0) {
        return null;
    }

    const value = {
        setSlides,
        slides,
        setCurr,
        curr,
        setEditSlide,
        editedSlide,
        filters,
        slidesTemp,
        setSlidesTemp,
        activeFilter,
        setActiveFilter
    }

    return (
        <MyContext.Provider value={value}>
            {props.children}
        </MyContext.Provider>
    )
}

export default MyContextProvider